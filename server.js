const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const sequelize = new Sequelize('c9','root','',{
	dialect : 'mysql'
	
})

const Category = sequelize.define('category', {
	categoryName : Sequelize.STRING
})

const Song = sequelize.define('song', {
	songName : Sequelize.STRING,
	songArtist : Sequelize.STRING
})

Category.hasMany(Song, {foreignKey: 'categoryId'});
Song.belongsTo(Category, {foreignKey: 'categoryId'});


const app = express()
app.use(bodyParser.json())

app.get('/create', (req, res) => {
	sequelize.sync({force : true})
		.then(() => res.status(201).send('recreated all tables'))
		.catch(() => res.status(500).send('hm, that was unexpected...'))	
})

app.get('/categories', (req, res) => {
	Category.findAll()
		.then((results) => {
			res.status(200).json(results)
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))			
})

app.post('/categories', (req, res) => {
	Category.create(req.body)
		.then(() => {
			res.status(201).send('created')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.get('/categories/:id', (req, res) => {
	Category.findById(req.params.id)
		.then((result) => {
			if (result){
				res.status(200).json(result)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.put('/categories/:id', (req, res) => {
	Category.findById(req.params.id)
		.then((result) => {
			if (result){
				return result.update(req.body)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('modified')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.delete('/categories/:id', (req, res) => {
	Category.findById(req.params.id)
		.then((result) => {
			if (result){
				return result.destroy()
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('removed')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.get('/categories/:cid/songs', (req, res) => {
	Category.findById(req.params.cid)
		.then((result) => {
			if (result){
				return result.getChapters()
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then((results) => {
			res.status(200).json(results)
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.get('/categories/:cid/songs/:sid', (req, res) => {
	Category.findById(req.params.cid)
		.then((result) => {
			if (result){
				return result.getSongs({where : {id : req.params.sid}})
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then((result) => {
			if (result){
				res.status(200).json(result)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))	
})

app.post('/categories/:cid/songs', (req, res) => {
	Category.findById(req.params.cid)
		.then((result) => {
			if (result){
				let song = req.body
				song.categoryId = result.id
				return Song.create(song)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).json('created')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.put('/categories/:cid/songs/:sid', (req, res) => {
	Song.findById(req.params.sid)
		.then((result) => {
			if (result){
				return result.update(req.body)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('modified')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.delete('/categories/:cid/songs/:sid', (req, res) => {
	Song.findById(req.params.sid)
		.then((result) => {
			if (result){
				return result.destroy()
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('removed')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.listen(8080)
